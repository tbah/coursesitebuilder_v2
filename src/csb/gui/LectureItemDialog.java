/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csb.gui;

import csb.data.Assignment;
import csb.data.Course;
import csb.data.Lecture;
import static csb.gui.AssignmentItemDialog.COMPLETE;
import static csb.gui.CSB_GUI.CLASS_HEADING_LABEL;
import static csb.gui.CSB_GUI.CLASS_PROMPT_LABEL;
import static csb.gui.CSB_GUI.PRIMARY_STYLE_SHEET;
import static csb.gui.ScheduleItemDialog.ADD_SCHEDULE_ITEM_TITLE;
import static csb.gui.ScheduleItemDialog.EDIT_SCHEDULE_ITEM_TITLE;
import java.time.LocalDate;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 *
 * @author Thierno
 */
public class LectureItemDialog extends Stage {
    Lecture lecture;
    
    // GUI CONTROLS FOR OUR DIALOG
    GridPane gridPane;
    Scene dialogScene;
    Label headingLabel;
    Label topicLabel;
    TextField topicTextField;
    Label numberOfLectureLabel;
    ComboBox <Integer> numberOfLectureComboBox;
    Button completeButton;
    Button cancelButton;
    
    // THIS IS FOR KEEPING TRACK OF WHICH BUTTON THE USER PRESSED
    String selection;
    
    public static final String COMPLETE = "Complete";
    public static final String CANCEL = "Cancel";
    
    public static final String NUMBER_PROMPT = "Number of sessions: ";
    public static final String TOPIC_PROMPT = "Topic:";
    public static final String LECTURE_ITEM_HEADING = "Lecture Details";
    public static final String ADD_LECTURE_ITEM_TITLE = "Add New Lecture";
    public static final String EDIT_ASSIGNMET_ITEM_TITLE = "Edit Lecture";
    
    public LectureItemDialog(Stage primaryStage, Course course,  MessageDialog messageDialog) {       
        // MAKE THIS DIALOG MODAL, MEANING OTHERS WILL WAIT
        // FOR IT WHEN IT IS DISPLAYED
        initModality(Modality.WINDOW_MODAL);
        initOwner(primaryStage);
        
        // FIRST OUR CONTAINER
        gridPane = new GridPane();
        gridPane.setPadding(new Insets(10, 20, 20, 20));
        gridPane.setHgap(10);
        gridPane.setVgap(10);
        
        // PUT THE HEADING IN THE GRID, NOTE THAT THE TEXT WILL DEPEND
        // ON WHETHER WE'RE ADDING OR EDITING
        headingLabel = new Label(LECTURE_ITEM_HEADING);
        headingLabel.getStyleClass().add(CLASS_HEADING_LABEL);
    
        // NOW THE DESCRIPTION 
        topicLabel = new Label(TOPIC_PROMPT);
        topicLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
        topicTextField = new TextField();
        topicTextField.textProperty().addListener((observable, oldValue, newValue) -> {
            lecture.setTopic(newValue);
        });
        
        // AND THE DATE
       /* numberOfLectureLabel = new Label(NUMBER_PROMPT);
        numberOfLectureLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
        datePicker = new DatePicker();
        datePicker.setOnAction(e -> {
            if (datePicker.getValue().isBefore(course.getStartingMonday())
                    || datePicker.getValue().isAfter(course.getEndingFriday())) {
                // INCORRECT SELECTION, NOTIFY THE USER
                PropertiesManager props = PropertiesManager.getPropertiesManager();
                messageDialog.show(props.getProperty(CSB_PropertyType.ILLEGAL_DATE_MESSAGE));
            }             
            else {
                assignment.setDate(datePicker.getValue());
            }
        });*/
        
        // AND THE URL
        numberOfLectureLabel = new Label(NUMBER_PROMPT);
        numberOfLectureLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
        numberOfLectureComboBox = new ComboBox();
        //numberOfLectureComboBox.setValue(1);
        for(int i = 1; i<4; i++){
            numberOfLectureComboBox.getItems().add(i);
        }
        numberOfLectureComboBox.setOnAction(e->{
          lecture.setSessions(numberOfLectureComboBox.getValue());
            System.out.println("sessions: "+lecture.getSessions());
        });
        
        // AND FINALLY, THE BUTTONS
        completeButton = new Button(COMPLETE);
        cancelButton = new Button(CANCEL);
        
        // REGISTER EVENT HANDLERS FOR OUR BUTTONS
        EventHandler completeCancelHandler = (EventHandler<ActionEvent>) (ActionEvent ae) -> {
            Button sourceButton = (Button)ae.getSource();
            LectureItemDialog.this.selection = sourceButton.getText();
            LectureItemDialog.this.hide();
        };
        completeButton.setOnAction(completeCancelHandler);
        cancelButton.setOnAction(completeCancelHandler);

        // NOW LET'S ARRANGE THEM ALL AT ONCE
        gridPane.add(headingLabel, 0, 0, 2, 1);
        gridPane.add(topicLabel, 0, 1, 1, 1);
        gridPane.add(topicTextField, 1, 1, 1, 1);
        gridPane.add(numberOfLectureLabel, 0, 2, 1, 1);
        gridPane.add(numberOfLectureComboBox, 1, 2, 1, 1);
        
        gridPane.add(completeButton, 0, 3, 1, 1);
        gridPane.add(cancelButton, 1, 3, 1, 1);

        // AND PUT THE GRID PANE IN THE WINDOW
        dialogScene = new Scene(gridPane);
        dialogScene.getStylesheets().add(PRIMARY_STYLE_SHEET);
        this.setScene(dialogScene);
    }
    
    /**
     * Accessor method for getting the selection the user made.
     * 
     * @return Either YES, NO, or CANCEL, depending on which
     * button the user selected when this dialog was presented.
     */
    public String getSelection() {
        return selection;
    }
    
    public Lecture getLectureItem() { 
        return lecture;
    }
    public Lecture showAddLectureItemDialog() {
        // SET THE DIALOG TITLE
        setTitle(ADD_LECTURE_ITEM_TITLE);
        
        // RESET THE SCHEDULE ITEM OBJECT WITH DEFAULT VALUES
        lecture = new Lecture();
        
        // LOAD THE UI STUFF
        topicTextField.setText(lecture.getTopic());
        numberOfLectureComboBox.setValue(lecture.getSessions());
        //datePicker.setValue(initDate);
        //topicsTextField.setText(assignment.getTopics());
        
        // AND OPEN IT UP
        this.showAndWait();
        
        return lecture;
    }
    public void loadGUIData() {
        // LOAD THE UI STUFF
        topicTextField.setText(lecture.getTopic());
        numberOfLectureComboBox.setValue(lecture.getSessions());
        //datePicker.setValue(assignment.getDate());
        //topicsTextField.setText(assignment.getTopics());       
    }
    
    public boolean wasCompleteSelected() {
        return selection.equals(COMPLETE);
    }
    public void showEditLectureItemDialog(Lecture itemToEdit) {
        // SET THE DIALOG TITLE
        setTitle(EDIT_SCHEDULE_ITEM_TITLE);
        
        // LOAD THE SCHEDULE ITEM INTO OUR LOCAL OBJECT
        lecture = new Lecture();
        lecture.setTopic(itemToEdit.getTopic());
        lecture.setSessions(itemToEdit.getSessions());
       // lecture.setTopics(itemToEdit.getTopics());
        
        // AND THEN INTO OUR GUI
        loadGUIData();
               
        // AND OPEN IT UP
        this.showAndWait();
    }
    
}
