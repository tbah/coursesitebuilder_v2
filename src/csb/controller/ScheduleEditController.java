package csb.controller;

import static csb.CSB_PropertyType.REMOVE_ITEM_MESSAGE;
import csb.data.Assignment;
import csb.data.Course;
import csb.data.CourseDataManager;
import csb.data.Lecture;
import csb.data.ScheduleItem;
import csb.gui.AssignmentItemDialog;
import csb.gui.CSB_GUI;
import csb.gui.LectureItemDialog;
import csb.gui.MessageDialog;
import csb.gui.ScheduleItemDialog;
import csb.gui.YesNoCancelDialog;
import javafx.collections.ObservableList;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;

/**
 *
 * @author McKillaGorilla
 */
public class ScheduleEditController {
    ScheduleItemDialog sid;
    AssignmentItemDialog aid;
    LectureItemDialog lid;
    MessageDialog messageDialog;
    YesNoCancelDialog yesNoCancelDialog;
    
    public ScheduleEditController(Stage initPrimaryStage, Course course, MessageDialog initMessageDialog, YesNoCancelDialog initYesNoCancelDialog) {
        sid = new ScheduleItemDialog(initPrimaryStage, course, initMessageDialog);
        aid = new AssignmentItemDialog(initPrimaryStage, course, initMessageDialog);
        lid = new LectureItemDialog(initPrimaryStage, course, initMessageDialog);
        messageDialog = initMessageDialog;
        yesNoCancelDialog = initYesNoCancelDialog;
    }

    // THESE ARE FOR SCHEDULE ITEMS
    
    public void handleAddScheduleItemRequest(CSB_GUI gui) {
        CourseDataManager cdm = gui.getDataManager();
        Course course = cdm.getCourse();
        sid.showAddScheduleItemDialog(course.getStartingMonday());
        
        // DID THE USER CONFIRM?
        if (sid.wasCompleteSelected()) {
            // GET THE SCHEDULE ITEM
            ScheduleItem si = sid.getScheduleItem();
            
            // AND ADD IT AS A ROW TO THE TABLE
            course.addScheduleItem(si);
        }
        else {
            // THE USER MUST HAVE PRESSED CANCEL, SO
            // WE DO NOTHING
        }
    }
    public void handleAddAssignment(CSB_GUI gui){
        CourseDataManager cdm = gui.getDataManager();
        Course course = cdm.getCourse();
        aid.showAddAssignmentItemDialog(course.getStartingMonday());
        if (aid.wasCompleteSelected()) {
            // GET THE SCHEDULE ITEM
            //ScheduleItem si = sid.getScheduleItem();
            Assignment hw = aid.getAssignmentItem();
            
            // AND ADD IT AS A ROW TO THE TABLE
            course.addAssignment(hw);
        }
        else {
            // THE USER MUST HAVE PRESSED CANCEL, SO
            // WE DO NOTHING
        } 
    }
    
    public void handleEditScheduleItemRequest(CSB_GUI gui, ScheduleItem itemToEdit) {
        CourseDataManager cdm = gui.getDataManager();
        Course course = cdm.getCourse();
        sid.showEditScheduleItemDialog(itemToEdit);
        
        // DID THE USER CONFIRM?
        if (sid.wasCompleteSelected()) {
            // UPDATE THE SCHEDULE ITEM
            ScheduleItem si = sid.getScheduleItem();
            itemToEdit.setDescription(si.getDescription());
            itemToEdit.setDate(si.getDate());
            itemToEdit.setLink(si.getLink());
        }
        else {
            // THE USER MUST HAVE PRESSED CANCEL, SO
            // WE DO NOTHING
        }        
    }
    public void handleEditAssignmentItemRequest(CSB_GUI gui, Assignment itemToEdit) {
        CourseDataManager cdm = gui.getDataManager();
        Course course = cdm.getCourse();
        aid.showEditAssignmentItemDialog(itemToEdit);
        
        // DID THE USER CONFIRM?
        if (aid.wasCompleteSelected()) {
            // UPDATE THE SCHEDULE ITEM
            Assignment si = aid.getAssignmentItem();
            itemToEdit.setName(si.getName());
            itemToEdit.setDate(si.getDate());
            itemToEdit.setTopics(si.getTopics());
        }
        else {
            // THE USER MUST HAVE PRESSED CANCEL, SO
            // WE DO NOTHING
        }        
    }
    public void handleEditLectureItemRequest(CSB_GUI gui, Lecture itemToEdit) {
        CourseDataManager cdm = gui.getDataManager();
        Course course = cdm.getCourse();
        lid.showEditLectureItemDialog(itemToEdit);
        
        // DID THE USER CONFIRM?
        if (lid.wasCompleteSelected()) {
            // UPDATE THE SCHEDULE ITEM
            Lecture lec = lid.getLectureItem();
            itemToEdit.setTopic(lec.getTopic());
            itemToEdit.setSessions(lec.getSessions());
            //itemToEdit.setLink(si.getLink());
        }
        else {
            // THE USER MUST HAVE PRESSED CANCEL, SO
            // WE DO NOTHING
        }        
    }
    public void handleAddLecture(CSB_GUI gui){
       CourseDataManager cdm = gui.getDataManager();
        Course course = cdm.getCourse();
        lid.showAddLectureItemDialog();
        if (lid.wasCompleteSelected()) {
            // GET THE SCHEDULE ITEM
            //ScheduleItem si = sid.getScheduleItem();
            Lecture lc = lid.getLectureItem();
            
            // AND ADD IT AS A ROW TO THE TABLE
            course.addLecture(lc);
        }
        else {
            // THE USER MUST HAVE PRESSED CANCEL, SO
            // WE DO NOTHING
        } 
    }/**
     * To move up lecture
     * @param gui
     * @param itemToEdit 
     */
    public void hanleMoveUpLectureRequest(CSB_GUI gui, Lecture itemToEdit){
         CourseDataManager cdm = gui.getDataManager();
         Course course = cdm.getCourse();
         ObservableList<Lecture> lectures = course.getLectures();
         int index = getIndexOfLecture(lectures, itemToEdit);
         if( index>0){
            lectures.remove(itemToEdit);
            lectures.add(index-1, itemToEdit);
         }
        
    }/**
     * To moveDown Lecture
     * @param gui
     * @param itemToEdit 
     */
    public void hanleMoveDownLectureRequest(CSB_GUI gui, Lecture itemToEdit){
         CourseDataManager cdm = gui.getDataManager();
         Course course = cdm.getCourse();
         ObservableList<Lecture> lectures = course.getLectures();
         int index = getIndexOfLecture(lectures, itemToEdit);
         if( index<lectures.size()-1){
            lectures.remove(itemToEdit);
            lectures.add(index+1, itemToEdit);
         }
        
    }
    /**
     * To get the index of the lecture on the table
     * @param lectures
     * @param itemToEdit
     * @return 
     */
    private int getIndexOfLecture(ObservableList<Lecture> lectures, Lecture itemToEdit){
        int i=0;
        while(i<lectures.size()){
            if(lectures.get(i).equals(itemToEdit))
                break;
            i++;
        }
        return i;
    }
    /**
     * To removeLecture on the table
     * @param gui
     * @param itemToRemove 
     */
    public void handleRemoveLectureItemRequest(CSB_GUI gui, Lecture itemToRemove) {
        // PROMPT THE USER TO SAVE UNSAVED WORK
        yesNoCancelDialog.show(PropertiesManager.getPropertiesManager().getProperty(REMOVE_ITEM_MESSAGE));
        
        // AND NOW GET THE USER'S SELECTION
        String selection = yesNoCancelDialog.getSelection();

        // IF THE USER SAID YES, THEN SAVE BEFORE MOVING ON
        if (selection.equals(YesNoCancelDialog.YES)) { 
            gui.getDataManager().getCourse().removeLecture(itemToRemove);
        }
    }
    public void handleRemoveAssignmentItemRequest(CSB_GUI gui, Assignment itemToRemove) {
        // PROMPT THE USER TO SAVE UNSAVED WORK
        yesNoCancelDialog.show(PropertiesManager.getPropertiesManager().getProperty(REMOVE_ITEM_MESSAGE));
        
        // AND NOW GET THE USER'S SELECTION
        String selection = yesNoCancelDialog.getSelection();

        // IF THE USER SAID YES, THEN SAVE BEFORE MOVING ON
        if (selection.equals(YesNoCancelDialog.YES)) { 
            gui.getDataManager().getCourse().removeAssignment(itemToRemove);
        }
    }
    
    public void handleRemoveScheduleItemRequest(CSB_GUI gui, ScheduleItem itemToRemove) {
        // PROMPT THE USER TO SAVE UNSAVED WORK
        yesNoCancelDialog.show(PropertiesManager.getPropertiesManager().getProperty(REMOVE_ITEM_MESSAGE));
        
        // AND NOW GET THE USER'S SELECTION
        String selection = yesNoCancelDialog.getSelection();

        // IF THE USER SAID YES, THEN SAVE BEFORE MOVING ON
        if (selection.equals(YesNoCancelDialog.YES)) { 
            gui.getDataManager().getCourse().removeScheduleItem(itemToRemove);
        }
    }
}